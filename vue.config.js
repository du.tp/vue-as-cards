const path = require('path');
const DEFAULT_SETTING = require('./src/settings');

function resolve(dir) {
  return path.join(__dirname, dir);
}

// Page title
const name = DEFAULT_SETTING.title || 'Aspire cards';

module.exports = {
  publicPath: '/',
  outputDir: 'dist',
  lintOnSave: process.env.ENV === 'development',
  productionSourceMap: false,
  devServer: {
    open: true,
    overlay: {
      warnings: true,
      errors: true,
    },
  },
  configureWebpack: {
    name,
    devtool: 'source-map',
    resolve: {
      alias: {
        '@': resolve('src'),
      },
    },
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import "@/assets/styles/variables.scss";`,
      },
    },
  },
};

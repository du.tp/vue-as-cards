import { createApp } from 'vue';
import App from './App.vue';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';

import AsIcon from '@/components/AsIcon';
import store from './store';
import router from './router';
import '@/assets/styles/index.scss'; // global css

const app = createApp(App);

app.use(router);
app.use(store);
app.use(ElementPlus);
app.use(AsIcon);

app.mount('#app');

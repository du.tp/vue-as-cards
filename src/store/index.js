import { createStore } from 'vuex';
import getters from './getters';
import Cards from './Cards';

const modules = {};

modules.card = Cards;

const store = createStore({
  modules,
  getters,
});

export default store;

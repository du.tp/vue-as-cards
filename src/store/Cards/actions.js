export default {
  setCards({ commit }, payload) {
    commit('SET_CARDS', payload);
  },
  deleteCard({ commit }, id) {
    commit('DELETE_CARD', id);
  },
  setFreezeCards({ commit }, status) {
    commit('FREEZE_CARDs', status);
  },
};

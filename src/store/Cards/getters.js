export default {
  cards: (state) => {
    return state.cards;
  },
  freezeCards: (state) => {
    return state.freezeCards;
  },
};

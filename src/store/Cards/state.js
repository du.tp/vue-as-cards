export default {
  cards: [
    {
      id: '1',
      cardName: 'Mark Henry',
      cardNumber: '1903220631452020',
      expire: '12/2020',
      cvv: 124,
      backgroundColor: '#01D167',
    },
    {
      id: '2',
      cardName: 'Hien Duc',
      cardNumber: '1903220631452021',
      expire: '12/2021',
      cvv: 235,
      backgroundColor: '#536dff',
    },
    {
      id: '3',
      cardName: 'Thanh Nghiem',
      cardNumber: '1903220631452022',
      expire: '12/2022',
      cvv: 456,
      backgroundColor: '#01D167',
    },
  ],
  freezeCards: false,
};

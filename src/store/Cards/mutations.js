export default {
  SET_CARDS: (state, data) => {
    state.cards = [...state.cards, { ...data }];
  },
  DELETE_CARD: (state, id) => {
    const newCards = state.cards.filter((card) => card.id !== id);
    state.cards = [...newCards];
  },
  FREEZE_CARDs: (state, status) => {
    state.freezeCards = status;
  },
};

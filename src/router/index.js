import { createRouter, createWebHistory } from 'vue-router';

import Layout from '@/layout';

export const constantRoutes = [
  {
    path: '/',
    name: 'Home',
    component: Layout,
    redirect: '/cards',
    children: [
      {
        path: 'cards',
        component: () => import('@/views/cards'),
        name: 'Home',
        meta: { title: 'Home cards' },
      },
    ],
  },
  {
    path: '/:catchAll(.*)',
    name: 'NotFound',
    component: () => import('@/views/404'),
  },
];

const router = createRouter({
  history: createWebHistory(),
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes,
});

export default router;

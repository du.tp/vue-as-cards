import { replaceCard } from './replaceCard';
import { freezeCard } from './freezeCard';
import { account } from './account';
import { credit } from './credit';
import { directiveCard } from './directiveCard';
import { gpay } from './gpay';
import { logo } from './logo';
import { pay } from './pay';
import { payments } from './payments';
import { spendLimit } from './spendLimit';
import { transfer } from './transfer';
import { eyeShow } from './eye-show';
import { eyeHide } from './eye-hide';
import { transaction } from './transaction';
import { cardDetail } from './card-detail';
import { fileStorage } from './file-storage';
import { flight } from './flight';
import { megaphone } from './megaphone';

export default {
  account: account,
  'card-detail': cardDetail,
  credit: credit,
  'directive-card': directiveCard,
  'eye-show': eyeShow,
  'eye-hide': eyeHide,
  flight: flight,
  'freeze-card': freezeCard,
  'file-storage': fileStorage,
  megaphone: megaphone,
  gpay: gpay,
  logo: logo,
  pay: pay,
  payments: payments,
  'replace-card': replaceCard,
  'spend-limit': spendLimit,
  transfer: transfer,
  transaction: transaction,
};
